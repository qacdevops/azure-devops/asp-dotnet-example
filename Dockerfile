FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY . .
ENTRYPOINT ["dotnet", "bin/Debug/net5.0/myWebApp.dll"]
